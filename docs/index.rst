.. include:: readme.rst

User and Developer Guides
=========================
.. toctree::
    :maxdepth: 4
    :caption: Contents:
    :numbered:

    userguide.rst
    modules.rst
    changelog.rst
